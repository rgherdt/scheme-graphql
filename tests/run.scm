(cond-expand
  (chicken
   (import test
           (chicken load)))
  (else (error "test requires CHICKEN.")))

(cond-expand (chicken (load-relative "../src/main.scm"))
             (else))

;;; Schema definitions

(define-type-schema
  sample-schema
  (define-graphql-type Person
    ((id (ID))
     (name (String))
     (address (Address :nullable))))
  (define-graphql-type Address
    ((street (String :nullable))))
  (define-graphql-type query
    ((person (fn (id ID) Person)))))

#;
(define sample-schema
  (alist->hash-table
   `((person . ,Person))))

;;; Database mock and Resolver Map

(define db
  (make-hash-table-eq))

(define (add-person field parent args)
  (define id (cdr (assq 'id args)))
  (define name (cdr (assq 'name args)))
  (set! db
    (alist->hash-table
     (cons (cons id
                 (alist->hash-table `((id . ,id) (name . ,name))))
           (hash-table->alist db))))
  (hash-table-ref db id))

(define (fetch-person obj parent args)
  (let ((id (cdr (assq 'id args))))
    (hash-table-ref db id)))

(define res-map
  (alist->hash-table
   (list (cons 'person fetch-person)
         (cons 'addPerson add-person )
         (cons 'name (lambda (obj parent args)
                       (hash-table-ref parent 'name)))
         (cons 'id #f))))

;;; Tests

(test-begin)
(let* ((tokenizer
        (make-tokenizer
         (open-input-string
             "mutation {
                  addPerson(id : 0, name : \"John\") {
                  name
                  }
              }")))
       (doc (parse tokenizer))
       (result (resolve doc res-map)))
  (test #f (memq 'errors (hash-table-keys result)))
  (test "John"
        (hash-table-nested-ref result '(data addPerson name))))

(let* ((tokenizer
        (make-tokenizer
         (open-input-string
          "query { 
               person(id : 0) {
                   name
                   address
                   age
               }
          }")))
       (doc (parse tokenizer))
       (result (resolve doc res-map)))
  (test 2 (length (hash-table-ref result 'errors)))
  (test "John"
        (hash-table-nested-ref result '(data person name)))
  (test "null"
        (hash-table-nested-ref result '(data person address)))
  (let* ((validation-result (validate doc sample-schema))
         (errors (hash-table-ref validation-result 'errors)))
    (test 1 (length errors))))

(let* ((tokenizer
        (make-tokenizer
         (open-input-string
          "query { 
               person(id : 0) {
               name
               }
           }")))
       (doc (parse tokenizer)))
  (test #t (validate doc sample-schema)))

(let* ((tokenizer
        (make-tokenizer
         (open-input-string
          "query { 
               person(id : \"abc\") {
                   name
               }
          }")))
       (doc (parse tokenizer)))
  (let* ((validation-result (validate doc sample-schema))
         (errors (hash-table-ref validation-result 'errors)))
    (test 1 (length errors))))

(test-exit)
