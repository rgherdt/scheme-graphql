;;;; This macros generate datatypes to guide code validation and
;;;; generating a graphql scheme to deliver to the clients.

(define (id? x) (integer? x))

(define (basic-type? t)
  (member t (hash-table-keys base-type-table)))

(define base-type-table
  (alist->hash-table
   `((ID . ,integer?)
     (String . ,string?)
     (Int . ,integer?))))

(define-syntax define-type-schema
  (syntax-rules ()
    ((_ schema-name (define-graphql-type tname arg ...) ...)
     (define schema-name
       (alist->hash-table
        `((tname . ,(define-graphql-type tname arg ...))
          ...))))))

;;; assign to the given variable a hash-table constructed based
;;; on the given arguments.
;;; Example:
#|
(define-graphql-type person-type
  (id (ID ':nullable))
  (name (String ':nullable))
  (children (list-of (person-type ':nullable)
                      ':nullable))
  (getChild (fn (name (String ':nullable))
                person-type)))
|#
(define-syntax define-graphql-type
  (syntax-rules ()
    ((_ name
        ((field-name field-type ...) ...))
     (let ((pred (lambda (h)
                   (eq? (hash-table-ref h 'type-name)
                        'name))))
       (alist->hash-table
        (list (cons 'type-name 'name)
              (cons 'predicate pred)
              (cons 'field-name (transform-type field-type ...)) ...))))))

;;; This translates each field type declaration into a proper result
;;; data structure.
;;; A field-type-declaration can be of the form:
;;; (name [':nullable boolean])
;;; (array-of [':nullable boolean] (field-type-declaration))
;;; (fn (field-type-declaration) ... result-type
(define-syntax transform-type
  (syntax-rules (list-of fn :nullable)
    ((_ (type-name :nullable arg0 arg ...))
     (transform-type (type-name arg0 arg ... :nullable)))
    ((_ (type-name arg ... :nullable))
     (hash-table-merge! (transform-type (type-name arg ...))
                        (alist->hash-table
                         `((nullable . #t)))))
    ((_ (list-of type-def))
     (alist->hash-table
      (list (cons 'type (cons 'list-of
                              (transform-type type-def))))))
    ((_ (fn (name type-def) ... result))
     (alist->hash-table
      (list (cons 'type 'function)
            (cons 'arguments
                  (alist->hash-table
                   (list
                    (cons 'name (transform-type (type-def))))) ...)
            (cons 'result-type 'result))))
    ((_ (type-name))
     (alist->hash-table
      (list (cons 'type 'type-name))))))

(define (graphql-schema->string tdef)
  (define spaces-per-indent-level 2)
  (define (indent level)
    (make-string (* level spaces-per-indent-level) #\space))
  (define (format-argument arguments arg-name)
    (format "~a : ~a" arg-name
            (hash-table-nested-ref arguments `(,arg-name type))))
  (define (format-field fname field indent-level)
    (let ((type-name (hash-table-ref field 'type))
          (nullable (hash-table-ref/default field 'nullable #f)))
      (if (eq? type-name 'function)
          (format "~a~a~a: ~a~%"
                  (indent indent-level)
                  fname
                  (map (lambda (arg)
                         (format-argument
                          (hash-table-ref field 'arguments)
                          arg))
                       (hash-table-keys
                        (hash-table-ref field 'arguments)))
                  (hash-table-ref field 'result-type))
          (format "~a~a: ~a~a~%"
                  (indent indent-level)
                  fname
                  type-name
                  (if nullable "" "!")))))
  (define (format-type tname)
    (format "type ~a {~%~a}~%"
            tname
            (apply string-append
                   (map (lambda (fname)
                          (format-field fname
                                        (hash-table-nested-ref
                                         tdef
                                         (list tname fname))
                                        1))
                        (lset-difference eq?
                                         (hash-table-keys
                                          (hash-table-ref tdef tname))
                                         '(predicate type-name))))))
  (apply string-append
         (map format-type
              (lset-difference eq?
                               (hash-table-keys tdef)
                               '(predicate)))))
