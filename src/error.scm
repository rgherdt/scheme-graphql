(define-record-type <graphql-error>
  (make-graphql-error type message token)
  graphql-error?
  (type graphql-error-type)
  (message graphql-error-message)
  (token graphql-error-token))

(define (make-tokenize-error message)
  (make-graphql-error 'tokenize-error message #f))

(define (make-parse-error message)
  (make-graphql-error 'parse-error message #f))

(define (make-resolve-error message)
  (make-graphql-error 'resolve-error message #f))

(define (make-graphql-type-error message token)
  (make-graphql-error 'type-error message token))

(define (tokenize-error? err)
  (and (graphql-error? err)
       (eq? (graphql-error-type err) 'tokenize-error)))

(define (parse-error? err)
  (and (graphql-error? err)
       (eq? (graphql-error-type err) 'parse-error)))

(define (resolve-error? err)
  (and (graphql-error? err)
       (eq? (graphql-error-type err) 'resolve-error)))

(define (graphql-type-error? err)
  (and (graphql-error? err)
       (eq? (graphql-error-type err) 'type-error)))

(define (format-graphql-error err)
  (let* ((prefix-message
          (cond ((tokenize-error? err) "Tokenize error")
                ((parse-error? err) "Parse error")
                ((resolve-error? err) "Resolve error")
                ((graphql-type-error? err) "Type error")
                (else (error "Not implemented."))))
         (token (graphql-error-token err))
         (locations
          (if token
              (list `(locations . (,(token->location token))))
              '())))
    (alist->hash-table
     `((errors .
               ,(list
                 (alist->hash-table
                  (append (list
                           `(message . ,(format "~a: ~a"
                                                prefix-message
                                                (graphql-error-message err))))
                          locations))))))))

(define (error-string->graphql-response msg)
  (alist->hash-table
   `((errors . ,(list (alist->hash-table
                       `((message . ,msg))))))))

