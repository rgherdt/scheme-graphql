;;; Token manipulation

(define current-token
  (make-parameter values))

(define tokenizer-function
  (make-parameter values))

(define (advance-token)
  (define new-tok ((tokenizer-function)))
  (current-token new-tok))

;;; Parse-tree objects

(define-record-type <document>
  (make-document definitions)
  document?
  (definitions document-definitions))

(define-record-type <operation-definition>
  (make-operation-definition op-type name var-defs dirs sel-set)
  operation-definition?
  (op-type operation-definition-type)
  (name operation-definition-name)
  (var-defs operation-definition-variable-definitions)
  (dirs operation-definition-directives)
  (sel-set operation-definition-selection-set))

(define-record-type <field>
  (make-field alias name args dirs sel-set)
  field?
  (alias field-alias)
  (name field-name)
  (args field-arguments)
  (dirs field-directives)
  (sel-set field-selection-set))

(define-record-type <argument>
  (make-argument name value)
  argument?
  (name argument-name)
  (value argument-value))

;;; Predicates

(define (executable-definition? req)
  (operation-definition? req))

(define (executable-definition-token? tok)
  (or (operation-definition-token? tok)
      (fragment-definition-token? tok)))

(define (operation-definition-token? tok)
  (operation-type-token? tok))

(define (fragment-definition-token? tok)
  (raise (make-parse-error
          "fragment-definition-token?: not implemented")))

(define (operation-type-token? tok)
  (any (lambda (x)
         (token-value=? tok x))
       '(query mutation subscription)))

(define (query? tok)
  (operation-definition? tok))

(define (mutation? tok)
  (operation-definition? tok))

;;; Parsing functions

(define (parse tokenizer)
  (define tok (tokenizer))
  (parameterize ((tokenizer-function tokenizer)
                 (current-token tok))
    (guard (condition
            ((parse-error? condition)
             (format-graphql-error condition))
            (else (raise condition)))
      (parse-document))))

(define (parse-document)
  (define definitions
    (let loop ((def (parse-definition))
               (res '()))
      (if (and def
               (not (eof-object? def)))
          (loop (parse-definition)
                (cons def res))
          res)))
  (unless (eof-object? (current-token))
    (raise
     (make-parse-error
      (string-append
       "Parsing ended before reaching <eof>."
       (format "seeing until now: ~a" definitions)
       (if (token? (current-token))
           (format "token name=~a value=~a~%"
                   (token-name (current-token))
                   (token-value (current-token)))
           (format "char ~a" (current-token)))))))
  (make-document (reverse definitions)))

(define (parse-definition)
  (cond ((operation-type-token? (current-token))
         (parse-operation-definition))
        (else #f)))

(define (parse-operation-definition)
  (define type (parse-operation-type))
  (define name (parse-name))
  (define selection-set (parse-selection-set))
;  (define variable-definitions (parse-variable-definitions))
;  (define directives (parse-directives))
;  (define tok (advance-token))
  (advance-token) ; consume closing }
  (if (and (not type)
           (null? selection-set))
      (raise
       (make-parse-error
        (string-append "parse-operation-definition: missing required fields: "
                       "operation-type selection-set")))
      (make-operation-definition type name #f #f selection-set)))

(define (parse-operation-type)
  (apply-parse operation-type-token?))

(define (parse-name)
  (apply-parse (lambda (tok) (token-name=? tok 'NAME))))

(define (parse-selection-set)
  (apply-parse-of-list #\{ parse-selection))

(define (parse-selection)
  (if (token-value=? (current-token) #\})
      #f
      (parse-field)))

(define (parse-field)
  (define (valid-field? f)
    (field-name f))
  (define field
    (make-field (parse-alias)
                (parse-name)
                (parse-arguments)
                (parse-directives)
                (parse-selection-set)))
  (if (valid-field? field)
      field
      #f))

(define (parse-alias)
  #f)

(define (parse-arguments)
  (apply-parse-of-list #\( parse-argument))

(define (parse-argument)
  (define name (parse-name))
  (define colon (parse-char #\:))
  (define value (parse-value))
  (parse-char #\,)
  (if (and name colon value)
      (make-argument name value)
      #f))

(define (parse-value)
  (define tok (current-token))
  (cond ((or (token-name=? tok 'INT-VALUE)
             (token-name=? tok 'FLOAT-VALUE)
             (token-name=? tok 'STRING-VALUE))
         (begin (advance-token)
                tok))
        ((token-value=? tok #\[)
         (begin (advance-token)
                (let loop ((tok (current-token))
                           (res '()))
                  (cond ((token-value=? tok #\])
                         (begin (advance-token)
                                res))
                        ((eof-object? tok)
                         (raise (make-parse-error "missing closing ]")))
                        (else (let ((val (parse-value)))
                                (loop (current-token)
                                      (cons val res))))))))
        ((token-value=? tok #\,)
         (advance-token)
         (parse-value))
        (else #f)))

(define (parse-directives)
  #f)

(define (parse-char char)
  (define tok (current-token))
  (if (token-value=? tok char)
      (begin (advance-token)
             tok)
      #f))

(define (parse-eof-object)
  (if (eof-object? (current-token))
      #t
      (raise (make-parse-error "parse-eof-object: query doesn't end with eof"))))

(define (apply-parse f)
  (define tok (current-token))
  (if (f tok)
      (begin (advance-token)
             tok)
      #f))

(define (apply-parse-of-list opening-char parsing-fn)
  (define tok (current-token))
  (if (not (token-value=? tok opening-char))
      '()
      (begin
        (advance-token)
        (let loop ((tok (parsing-fn))
                   (result '()))
          (if tok
              (loop (parsing-fn)
                    (cons tok result))
              (begin (advance-token) ; consume closing char
                     result))))))

(define (parse-query)
  (define parameters
    (let loop ((tok (advance-token))
               (res '()))
      (cond ((token-value=? tok #\{)
             (reverse res))
            (else (loop (advance-token)
                        (cons tok res))))))
  (map show parameters))

;;; Validation

;; These functions operate using a type-definition generated by macros
;; defined in scheme.scm

;; validate an AST obtained by parse according to SCHEMA.
;; Return #t if AST is valid, otherwise return a GraphQL error.
(define (validate ast schema)
  (cond ((document? ast) (validate-parsed-document ast schema))
        (else (error "invalid argument" ast))))

(define (validate-parsed-document doc schema)
  (let ((definitions (document-definitions doc)))
    (unless (not (null? definitions))
      (raise (make-graphql-type-error "document has no definitions." #f)))
    (guard (condition
            ((graphql-type-error? condition)
             (format-graphql-error condition))
            (else (raise condition)))
      (all-true? (map (lambda (def)
                        (validate-query def schema))
                      definitions)))))

(define (validate-query query type-definition)
  (let ((query-fields (operation-definition-selection-set query)))
    (all-true? (map (lambda (field)
                      (validate-query-field field type-definition))
                    query-fields))))

(define (validate-query-field field type-definition)
  (define fname (token-value (field-name field)))
  (define (validate-arguments rst-args arg-defs)
    (if (null? rst-args)
        #t
        (let* ((arg (car rst-args))
               (arg-name (token-value (argument-name arg)))
               (arg-value (token-value (argument-value (car rst-args)))))
          (if (memq arg-name (hash-table-keys arg-defs))
              (let* ((type
                      (hash-table-nested-ref arg-defs `(,arg-name type)))
                     (pred (cond ((symbol? type)
                                  (hash-table-ref base-type-table type))
                                 ((procedure? type)
                                  type)
                                 ((and (list? type)
                                       (eq? (car type)
                                            'list-of))
                                  list?)
                                 (else
                                  (error "validation not implemented: " type)))))
                (if (pred arg-value)
                    (validate-arguments (cdr rst-args) arg-defs)
                    (raise
                     (make-graphql-type-error
                      (format "field ~a of the wrong type"
                              fname)
                      (argument-name arg)))))
              (raise (make-graphql-type-error
                      (format "field ~a doesn't have argument ~a"
                              fname
                              arg-name)
                      (argument-name arg)))))))
  (let* ((query-def (hash-table-ref/default type-definition 'query #f))
         (cur-query-def (hash-table-ref/default query-def fname #f))
         (arguments (field-arguments field)))
    (cond ((not cur-query-def)
           (raise (make-graphql-type-error
                   (format "query type for ~a doesn't exist" fname)
                   (field-name field))))
          ((let* ((result-type (hash-table-ref/default cur-query-def
                                                       'result-type
                                                       empty-hash-table))
                  (tdef (hash-table-ref/default type-definition
                                                result-type
                                                #f)))
             (and tdef
                  (validate-arguments arguments
                                      (hash-table-ref cur-query-def
                                                      'arguments))
                  (every (lambda (f)
                           (validate-field f tdef))
                         (field-selection-set field))))
           #t)
          (else (raise '(unknown))))))

;; a field is valid if its contained in its parent type definition,
;; and it's well typed
(define (validate-field field type-definition)
  (define fname (token-value (field-name field)))
  (let* ((tdef (hash-table-ref/default type-definition fname #f)))
    (cond ((not tdef)
           (raise (make-graphql-type-error
                   (format "parent type doesn't have field ~a" fname)
                   (field-name field))))
          ((every (lambda (f)
                    (validate-field f tdef))
                  (field-selection-set field))
           #t)
          (else (raise '(unknown))))))


;;; Utility

(define (pretty-print-ast ast)
  (define spaces-per-indent-level 4)
  (define (indent level)
    (make-string (* level spaces-per-indent-level) #\space))
  (let loop ((indent-level 0)
             (node ast))
    (cond ((document? node)
           (format "~adocument~%~a~%"
                   (indent indent-level)
                   (apply string-append
                          (map (lambda (def)
                                 (loop (+ indent-level 1) def))
                               (document-definitions node)))))
          ((operation-definition? node)
           (format "~aoperation-definition: ~a~%~a"
                   (indent indent-level)
                   (token-value (operation-definition-type node))
                   (apply string-append
                          (map (lambda (sel)
                                 (loop (+ indent-level 1)
                                       sel))
                               (operation-definition-selection-set node)))))
          ((field? node)
           (if (null? (field-selection-set node))
               (format "~afield: ~a~%~aarguments:~%~a"
                       (indent indent-level)
                       (token-value (field-name node))
                       (indent (+ 1 indent-level))
                       (apply string-append
                              (map (lambda (arg)
                                     (loop (+ indent-level 1)
                                           arg))
                                   (field-arguments node))))
               (format "~afield: ~a~%~aarguments:~%~a~a"
                       (indent indent-level)
                       (token-value (field-name node))
                       (indent (+ 1 indent-level))
                       (apply string-append
                              (map (lambda (arg)
                                     (loop (+ indent-level 2)
                                           arg))
                                   (field-arguments node)))
                       (apply string-append
                              (map (lambda (sel)
                                     (loop (+ indent-level 1)
                                           sel))
                                   (field-selection-set node))))))
          ((argument? node)
           (format "~aargument: (~a ~a)~%"
                   (indent indent-level)
                   (token-value (argument-name node))
                   (token-value (argument-value node))))
          (else (error "pretty-print-ast: not implemented" node)))))
