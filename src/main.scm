(import (scheme char)
        (scheme write)
        (scheme base))

(cond-expand
  (chicken (import r7rs
                   srfi-1
                   srfi-28
                   srfi-69))
  (guile (import  (scheme base)
                  (srfi srfi-1)
                  (srfi srfi-28)
                  (srfi srfi-69)))
  (chibi (import (srfi 1)
                 (srfi 69)))
  (racket (import (only (srfi/1) fold any)
                  (srfi/28)
                  (srfi/69))))

(cond-expand
  (guile ; original implementation is currently buggy (patch sent)
   (define (hash-table-merge! ht other-ht)
     (hash-table-fold
      other-ht (lambda (k v ign) (hash-table-set! ht k v)) #f)
     ht))
  (else))

(include "util.scm")
(include "error.scm")
(include "token.scm")
(include "parser.scm")
(include "resolver.scm")
(include "schema.scm")
