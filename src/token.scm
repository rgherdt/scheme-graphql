
(define-record-type <token>
  (make-token name value line column)
  token?
  (name token-name)
  (value token-value)
  (line token-line)
  (column token-column))

(define-record-type <position>
  (make-position line column)
  position?
  (line position-line)
  (column position-column))

(define (newline? ch)
  (char=? ch #\newline))

(define (make-tokenizer port)
  (define line 1)
  (define column 1)

  (define (next-token)
    (define ch (skip-whitespace))
    (cond ((eof-object? ch) ch)
          ((char-alphabetic? ch) (read-name))
          ((or (char=? ch #\-)
               (char-numeric? ch)) (read-number))
          ((char=? ch #\") (read-a-string))
          ((char? ch) (read-a-char))
          (else (error "next-token: character not recognized" ch))))

  ;; NAME starts with an alphabetic char and may contain numeric chars
  (define (read-name)
    (define (make-name-token val)
      (make-token 'NAME
                  (string->symbol (list->string (reverse val)))
                  line
                  column))
    (let loop ((ch (peek))
               (result '()))
      (cond ((eof-object? ch)
             (make-name-token result))
            ((or (char-alphabetic? ch)
                 (char-numeric? ch)
                 (char=? ch #\_))
             (let ((next-char (read-a-char)))
               (loop (peek)
                     (cons next-char result))))
            (else (make-name-token result)))))

  ;; can be integer or float. We don't distinguish the token name to
  ;; keep parsing simple.
  (define (read-number)
    (define (make-number-token val)
      (let ((number
             (string->number (list->string (reverse val)))))
        (make-token (if (exact? number)
                        'INT-VALUE
                        'FLOAT-VALUE)
                    number
                    line
                    column)))
    (let loop ((ch (peek))
               (result '()))
      (cond ((eof-object? ch)
             (make-number-token result))
            ((and (char? ch)
                  (or (char-numeric? ch)
                      (member ch '(#\e #\E #\+ #\- #\.))))
             (let ((c (read-a-char)))
               (loop (peek)
                     (cons c result))))
            (else (make-number-token result)))))

  (define (read-a-string)
    (define (make-string-token val)
      (make-token 'STRING-VALUE
                  (list->string (reverse val))
                  line
                  column))
    (define st (stack))
    (let loop ((ch (peek))
               (result '())
               (state 'accumulating))
      (cond ((eof-object? ch)
             (unless (and (empty-stack? st)
                          (eq? state 'removing))
               (error "read-a-string: reached <eof> before ending string."
                      state (show-stack st) result ch))
             (make-string-token result))
            ((eq? state 'accumulating)
             (let ((c (read-a-char)))
               (if (eq? c #\")
                   (begin
                     (push! st c)
                     (loop (peek) result 'accumulating))
                   (loop (peek) (cons c result) 'reading-text))))

            ((eq? state 'reading-text)
             (let ((c (read-a-char)))
               (if (eq? c #\")
                   (begin
                     (pop! st)
                     (loop (peek) result 'removing))
                   (loop (peek) (cons c result) 'reading-text))))

            ((eq? state 'removing)
             (cond ((and (char? ch)
                         (char=? ch #\"))
                    (if (empty-stack? st)
                        (error "read-a-string: too many closing \""
                               state (show-stack st) ch)
                        (begin
                          (read-a-char)
                          (pop! st)
                          (loop (peek) result 'removing))))
                   ((empty-stack? st)
                    (make-string-token result))
                   (else (error "read-a-string: nested \" not allowed."
                                state (show-stack st) ch))))
            
            (else (error
                   "read-a-string: invalid string"
                   state (show-stack st) ch)))))

  (define (inc-line!)
    (set! line (+ 1 line)))
  (define (inc-column!)
    (set! column (+ 1 column)))

  (define (read-a-char)
    (define (inc-position! ch)
      (cond ((newline? ch)
             (inc-line!)
             (set! column 0))
            (else (inc-column!))))
    (let ((ch (read-char port)))
      (unless (eof-object? ch)
        (inc-position! ch))
      ch))

  (define (skip-whitespace)
    (let ((ch (peek)))
      (cond ((and (char? ch) (char-whitespace? ch))
             (read-a-char)
             (skip-whitespace))
            (else ch))))

  (define (peek) (peek-char port))
  next-token)

(define (token-value=? tok x)
  (cond ((char? tok)
         (if (char? x)
             (char=? tok x)
             #f))
        ((eof-object? tok) (eof-object? x))
        (else
         (let ((val (token-value tok)))
           (cond ((and (string? val) (string? x))
                  (string=? val x))
                 ((and (or (symbol? val)
                           (number? val))
                       (or (symbol? x)
                           (number? x)))
                  (eq? val x))
                 (else #f))))))

(define (token-name=? tok x)
  (cond ((char? tok) #f)
        ((eof-object? tok) #f)
        (else (let ((name (token-name tok)))
                (cond ((string? name)
                       (string=? name x))
                      ((or (symbol? name)
                           (number? name))
                       (eq? name x))
                      (else "token-name=?: value not known" name))))))

(define (show-token tk)
  (format "(token ~a ~a)"
          (token-name tk)
          (token-value tk)))

(define (show x)
  (cond ((token? x) (show-token x))
        (else (format "~a" x))))

(define (token->location token)
  (alist->hash-table
   `((line . ,(token-line token))
     (column . ,(token-column token)))))
