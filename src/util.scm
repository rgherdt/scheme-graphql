(define-record-type <stack>
  (make-stack contents size)
  stack?
  (contents stack-contents set-stack-contents!)
  (size stack-size set-stack-size!))

(define (stack) (make-stack '() 0))
(define (empty-stack? st) (= (stack-size st) 0))
(define (push! st x)
  (set-stack-contents! st
                       (cons x (stack-contents st)))
  (set-stack-size! st (+ 1 (stack-size st)))
  'ok)
(define (pop! st)
  (unless (not (empty-stack? st))
    (error "pop!: trying to pop an element of an empty stack."))
  (let* ((contents (stack-contents st))
         (x (car contents)))
    (set-stack-contents! st (cdr contents))
    (set-stack-size! st (- 1 (stack-size st)))
    x))
(define (show-stack st)
  (string-append "size: "
                 (number->string (stack-size st))
                 " contents: "
                 (list->string (stack-contents st))))

(define (pretty-print-graphql-result res)
  (define spaces-per-indent-level 2)
  (define (indent level)
    (make-string (* level spaces-per-indent-level) #\space))
  (define (pprint elem level)
    (cond ((hash-table? elem)
           (pprint-hash-table elem level))
          ((list? elem)
           (pprint-list elem level))
          (else (format "~a~a" (indent level) elem))))
  (define (pprint-list lst level)
    (string-append
     (format "~a[~%" (indent level))
     (apply string-append
            (map (lambda (e)
                   (format "~a~%"
                           (pprint e (+ 1 level))))
                 lst))
     (format "~a]" (indent level))))
  (define (pprint-hash-table hash level)
    (string-append
     (format "~a{~%" (indent level))
     (hash-table-fold hash
                      (lambda (k v prev)
                        (string-append
                         (if (or (hash-table? v) (list? v))
                             (format "~a~a :~%~a~%"
                                     (indent (+ level 1))
                                     k
                                     (pprint v (+ level 1)))
                             (format "~a~a : ~a~%"
                                     (indent (+ level 1)) k v))
                         prev))
                      "")
     (format "~a}" (indent level))))
  (pprint res 0))

(define (join-hash-tables lst)
  (define new-ht (make-hash-table-eq))
  (fold (lambda (h acc)
            (hash-table-merge! acc h))
        new-ht
        lst))

;; return the value of nested hash tables addressed by keys
(define (hash-table-nested-ref hash keys)
  (let loop ((h hash)
             (k keys))
    (cond ((null? k) h)
          ((null? (cdr k))
           (hash-table-ref h (car k)))
          ((hash-table? h)
           (loop (hash-table-ref h (car k))
                 (cdr k)))
          (else #f))))

(define (all-true? lst)
  (every (lambda (x) (eq? x #t)) lst))

(define (make-hash-table-eq)
  (make-hash-table eq? hash-by-identity))

(define empty-hash-table (make-hash-table-eq))

(cond-expand
  (chibi
   (define format
     (lambda (format-string . objects)
       (let ((buffer (open-output-string)))
         (let loop ((format-list (string->list format-string))
                    (objects objects))
           (cond ((null? format-list) (get-output-string buffer))
                 ((char=? (car format-list) #\~)
                  (if (null? (cdr format-list))
                      (error 'format "Incomplete escape sequence")
                      (case (cadr format-list)
                        ((#\a)
                         (if (null? objects)
                             (error 'format "No value for escape sequence")
                             (begin
                               (display (car objects) buffer)
                               (loop (cddr format-list) (cdr objects)))))
                        ((#\s)
                         (if (null? objects)
                             (error 'format "No value for escape sequence")
                             (begin
                               (write (car objects) buffer)
                               (loop (cddr format-list) (cdr objects)))))
                        ((#\%)
                         (newline buffer)
                         (loop (cddr format-list) objects))
                        ((#\~)
                         (write-char #\~ buffer)
                         (loop (cddr format-list) objects))
                        (else
                         (error 'format "Unrecognized escape sequence")))))
                 (else (write-char (car format-list) buffer)
                       (loop (cdr format-list) objects))))))))
  (else))
