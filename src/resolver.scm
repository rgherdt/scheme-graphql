(define *resolver-map*
  (make-parameter (make-hash-table-eq)))

(define (resolve request resolver-map)
  (parameterize ((*resolver-map* resolver-map))
    (guard (condition
            ((resolve-error? condition)
             (format-graphql-error condition))
            (else (raise condition)))
      (resolve-document request))))

(define (resolve-document req)
  (join-hash-tables
   (map resolve-definition
        (document-definitions req))))

(define (resolve-definition req)
  (cond ((executable-definition? req)
         (resolve-executable-definition req))
        (else
         (raise (make-resolve-error
                 (format "resolve-definition: not implemented: ~a" req))))))

(define (resolve-executable-definition req)
  (cond ((operation-definition? req)
         (resolve-operation req))
        (else
         (raise (make-resolve-error
                 (format "resolve-executable-definition: not implemented: ~a"
                         req))))))

(define (resolve-operation req)
  (cond ((or (query? req)
             (mutation? req))
         (resolve-query-or-mutation req))
        (else
         (raise (make-resolve-error
                 (format "resolve-operation: not implemented: ~a"
                         req))))))

(define (resolve-query-or-mutation query)
  (define op-name (operation-definition-name query))
  (define resolve-result
    (resolve-selection-set (operation-definition-selection-set query)
                           #f
                           '()))
  (define resolved-sel-set
    (car resolve-result))
  (define errors (cadr resolve-result))
  (cond ((null? (hash-table-keys resolved-sel-set))
         (alist->hash-table `((errors . ,errors))))
        ((null? errors)
         (alist->hash-table `((data . ,resolved-sel-set))))
        (else
         (alist->hash-table `((data . ,resolved-sel-set)
                              (errors . ,errors))))))

(define (resolve-selection-set selection-set parent path)
  (if (null? selection-set)
      (list (make-hash-table-eq) '())
      (let loop ((sel-set selection-set)
                 (resolved-data '())
                 (errors '()))
        (cond ((null? sel-set)
               (list (join-hash-tables resolved-data) errors))
              (else
               (let* ((sel (car sel-set))
                      (resolve-result
                       (resolve-selection sel parent path)))
                 (loop (cdr sel-set)
                       (cons (car resolve-result) resolved-data)
                       (append (cadr resolve-result) errors))))))))

(define (resolve-selection selection parent path)
  (guard (condition
          ((resolve-error? condition)
           (list
            (alist->hash-table
             `((,(token-value (field-name selection)) . "null")))
            (list
             (alist->hash-table
              `((message . ,(graphql-error-message condition))
                (locations .
                           ,(alist->hash-table
                             `((line . ,(token-line (field-name selection)))
                               (column . ,(token-column (field-name selection))))))
                (path . ,(reverse
                          (cons (symbol->string
                                 (token-value (field-name selection)))
                                path))))))))
          (else (raise condition)))
    (resolve-field selection parent path)))


(define (resolve-field field parent path)
  (define name (token-value (field-name field)))
  (define new-path (cons (symbol->string name) path))
  (define resolver
    (let ((res (hash-table-ref (*resolver-map*) name (lambda () #f))))
      (if res
          res
          default-resolver)))
  (define resolved-object
    (resolver name parent
              (map (lambda (arg)
                     (cons (token-value (argument-name arg))
                           (token-value (argument-value arg))))
                   (field-arguments field))))
  (cond ((hash-table? resolved-object)
         (let ((resolve-result
                (resolve-selection-set (field-selection-set field)
                                              resolved-object
                                              new-path)))
           (list
            (alist->hash-table
             `((,name . ,(car resolve-result))))
            (cadr resolve-result))))
        (else
         (list (alist->hash-table `((,name . ,resolved-object)))
               '()))))

(define (default-resolver field parent args)
  (define parent-field (hash-table-ref parent field (lambda () #f)))
  (cond ((and parent-field
              (procedure? parent-field))
         (apply parent-field args))
        (parent-field parent-field)
        (else
         (let ((err (make-resolve-error
                     (format "field ~a not found" field))))
           (raise err)))))

