(define-library (graphql)
  (export parse
          pretty-print-graphql-result
          resolve
          define-type-schema
          define-graphql-type
          error-string->graphql-response
          make-tokenizer
          transform-type
          validate
          graphql-schema->string)

  (include "main.scm"))
